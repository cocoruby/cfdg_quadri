# Quadrillage avec cercle central

## quadri.cfdg

Tests manuels 

## quadri_vars.cfdg

Avec variabilisation du nombre de lignes et dégradé sur l'épaisseur de celles-ci.  
Dégradé inversé entre les lignes droites et les arcs.

## Compilation

```bash
cfdg --size=2000x2000 quadri_vars.cfdg quadri_vars.png
```


